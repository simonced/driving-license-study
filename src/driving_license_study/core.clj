(ns driving-license-study.core
  (:gen-class))

(require '[clojure.string :as str])


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World! 2"))


  (defn line-valid? [line_]
    "We filter the lines we want, there should be some pattern to them"
    (if (nil? (re-matches #"^#.*" line_))
        (if (not (nil? (re-matches #"^(.*?):.*\1.*" line_)))
          true
          (if (not (nil? (re-matches #"^[TF]:.*" line_)))
            true
            false)
        )
      false
      ))


(defn parse [content_]
  "Parse file content and keep questions (ignoring comments and non valid lines)."
  (let [lines (str/split content_ #"\n")]
  (filter line-valid? lines)
  ))


(defn ask [question]
  "will ask the question and wait for an answer T or F"
  ; TODO
  )