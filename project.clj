(defproject driving-license-study "0.1.0-SNAPSHOT"
  :description "This is just a little application to throw driving license test questions at you!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot driving-license-study.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
