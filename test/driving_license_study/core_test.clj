(ns driving-license-study.core-test
  (:require [clojure.test :refer :all]
            [driving-license-study.core :refer :all]))


(def test-statement "#sample of different patterns
T:This is a true statement.
F:This is a false statement.
30:Mopeds can drive at {20,30,50}Km/h on non signalized roads.
This statement is invalid.")


(deftest a-test
  (testing "listing raw questions while ignoring comments (starting with #)"
    (is (= 3 (count (driving-license-study.core/parse test-statement))))))
